/* Is it okay to state all the methods as static to apply them into the main? As far as 
I remember, you declare the methods without using "static". Apart from that, do you have 
any advise to improve the code, thank you for your reply.  */

package CS102_Labs;

public class FirstLab {
    public static double x;
    public static double y;
    public static double r;
    public static double pi = 3.14;
    public static double dx;
    public static double dy;

    public static double getX() {
        return x; }
    public static double getY() {
        return y; }
    public static double getR() {
        return r; }
    public static double getArea() {
        return pi * r * r; }

    public static void setX(double newX) {
        x = newX; }
    public static void setY(double newY) {
        y = newY; }
    public static void setR(double newR) {
        r = newR; }
    
    public static void translate(double dx, double dy) {
        x = x + dx;
        y = y + dy; }
    public static double scale(double scaler) {
        r = r * scaler;
        return r;   }
         
    public static void main(String[] args) {

        x = -2.06;
        y = -0.23;
        r = 1.33;

        getX();
        getY();
        getR();
        getArea();
        System.out.println("X: " + getX() + " Y: " + getY() + " R: " + getR() + " Area of the circle: " + getArea());

        setX(0);
        setY(5);
        setR(1.88);
        getArea();
        System.out.println("X: " + getX() + " Y: " + getY() + " R: " + getR() + " Area of the circle: " + getArea());

        translate(5, 0);
        System.out.println("X: " + getX() + " Y: " + getY() + " R: " + getR() + " Area of the circle: " + getArea());

        scale(0.5);
        System.out.println("X: " + getX() + " Y: " + getY() + " R: " + getR() + " Area of the circle: " + getArea());

        translate(-1, -1);
        setR(3.1);
        System.out.println("X: " + getX() + " Y: " + getY() + " R: " + getR() + " Area of the circle: " + getArea());
        }
    
    }

